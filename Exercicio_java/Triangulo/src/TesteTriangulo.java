import java.util.Scanner;
import javax.swing.JOptionPane;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Axel_
 */
public class TesteTriangulo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
                Triangulo t1,t2;
        //objt com construtor c/ parametros
        t1 = new Triangulo(10, 7);
        t1.setAltura(155);
        t1.setBase(155);
        System.out.println("Os dados do triangulo é" + t1.imprimeDados());
        
        //objt com construtor padrao
        t2 = new Triangulo();
        Scanner leia = new Scanner(System.in);
        
        System.out.println("Insira a base desse triangulo");
        t2.setBase(leia.nextDouble());
        System.out.println("Insira a altura desse triangulo");
        t2.setAltura(leia.nextDouble());
        
        System.out.println("Dados do triangulo:" + t2.imprimeDados());
       
    }
    
}
