/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno
 */
public class Triangulo {
    //atributos
    private double base;
    private double altura;
    //construtores 

    public Triangulo() {
    }

    public Triangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }
    
    //metodos acessores

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
    //metodos 
    public double calculaArea(){
        double area = this.altura * this.base / 2;
        return area;
    }
    
    public String imprimeDados(){
        String texto = "A area desse triangulo é:  "+ this.calculaArea();
          return texto;      
    }
    
    
}
