# AlunoTPP


Repositório de exercícios das aulas de Técnicas de Programação


[Exercícios] Git
Crie um novo repositório local com um projeto à sua escolha e realize pelo menos dois commits
neste repositório
Escolha um repositório disponível no gitlab.com e faça a clonagem do mesmo, identificando qual
foi o autor do último commit realizado no projeto e a(s) linguagem(s) utilizadas.
Identifique a finalidade dos seguintes comandos:
a) git init
b) git config --global user.name "turing"
c) git add EXERCICIO.txt
d) git add .
e) git commit -m "Adicionado nova interface"
f) git commit
g) git reset --hard HEAD
h) cd Downloads
i) pwd
j) cd ..
k) ls
l) git pull
m) git push
n) git clone https://gitlab.com/rVenson/linguagemdeprogramacao
o) git diff
p) git show
Descreva a função dos seguintes componentes do Git
1) Stage Area, Commit
2) Local Repository
3) Remote Repository
Exercício 2
Crie seu próprio repositório no GitLab denominado AlunoTP.
Crie uma pasta chamada projeto e exercicios e na pasta exercícios inclua a resolução dos
exercícios Git.
Envie o commit para o repositório remoto.
Adicione um arquivo denominado README.md ao projeto e inclua o seguinte conteúdo
Repositório de exercícios das aulas de Técnicas de Programação
Adicione uma nova pasta dentro da pasta exercícios chamado exercicio_java. Busque um
exercício que você já realizou e suba para o repositório.
Crie também um arquivo chamado README.md na pasta raiz da pasta e inclua o cabeçalho deste
exercício.
